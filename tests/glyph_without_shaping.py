# Report glyphs (with no Unicode code point) without shaping rules associated in a font
#
# Copyright 2021 Rajeesh KV <rajeeshknambiar@gmail.com>
# This file is licensed under GPLv3.

# Usage: $0 font1 [font2 ...]

import sys, os
import fontforge as ff

IGNORE_GLYPHS = [".notdef", ".null", "NULL", "r4", "l4"]
COLR_OK = '\033[92m'    #Green
COLR_WARN = '\033[93m'  #Amber
COLR_RESET = '\033[0m'

def report_glyphs_sans_shaping(font_file):
    if not os.path.exists(font_file):
        print(f"Font file {font_file} not found")
        return False
    font = ff.open(font_file)
    print("Checking glyphs without codepint and substitution rules in " + font.fontname)
    error_count = 0
    for glyph in font.glyphs():
        if glyph.unicode == -1 and glyph.glyphname not in IGNORE_GLYPHS:     #TODO: check glyph.altuni too?
            subrules = glyph.getPosSub("*")     #Returns (("lookupname", "LookupType", ...),), where LookupType is one of "Position", "Pair", "Substitution", "AltSubs", "MultSubs", "Ligature".
            lookuptypes = list(map(lambda i:i[1], subrules))
            # Stylistic alternate will have substitution rules on its main glyph
            if not lookuptypes and glyph.glyphname.endswith(".ss01"):
                #TODO: check if substitution is defined on main glyph
                continue
            if not any(l in ["Substitution", "MultSubs", "Ligature"] for l in lookuptypes):
                error_count += 1
                print ("\t" + COLR_WARN + str(glyph.originalgid) + ": " + glyph.glyphname + COLR_RESET + " has no Unicode codepoint or substitution rule")
    return error_count

if __name__ == "__main__":
    if len(sys.argv) > 1:
        error_count = 0
        for f in sys.argv[1:]:
            error_count += report_glyphs_sans_shaping(f)
        if error_count == 0:
            print(COLR_OK + "All glyphs have either Unicode value or GSUB rules associated" + COLR_RESET)
        sys.exit(error_count)
    else:
        print("Please pass some fonts for testing")
        sys.exit(1)
